﻿=== WP Flickr Album Gallery Widget & Shortcode - profile ===
Contributors: awordpresslife
Tags: flickr, gallery, flickr gallery, album gallery, flickr profile
Donate link: http://awplife.com/
Requires at least: 3.8
Tested up to: 4.9
Stable tag: 0.0.16
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Flickr Album Gallery Widget & Shortcode - Flickr profile plugin to create Responsive Simple Flickr Photostream & Album Gallery With Shortcode to publish your gallery anywhere in WordPress.

== Description ==

= Flickr Album Gallery Widget & Shortcode - Flickr profile =

This is the new “Flickr Gallery WordPress Plugin”, Who based on Irresistible CSS & JS, So it is very amazing and easy to use.

With Flickr gallery, you can show your flicker account photo albums and stream photos.

This is the best Plugin for any site Because this Flickr Gallery WordPress Plugin has many configurations to show your content, you can show your Flickr profile details on the site.

You can use it also in side bar like a widget, Just paste the short code in the text widget. this plugin easier to customize.

This plugin easier to customize, Just put your Flickr ID and your plugin will be ready for lunch, If you don’t know your Flickr ID, You can use our blog to get Flickr Id here is the link for Flickr ID – How To Get Flickr Id,

It will be very helpful for new users, this plug in very easy in use for new and old users of WordPress.

** HOW TO USE PLUGIN  ** 

https://www.youtube.com/watch?v=_MsJJEjrxpE

**FLICKR GALLERY PLUGIN FEATURES DEMO**

> * [Flickr Gallery Premium](http://demo.awplife.com/flickr-gallery-premium/ "Flickr Gallery Premium")
> * [Try Admin Demo](http://demo.awplife.com/flickr-gallery-premium-admin-demo/ "Try Admin Demo")
> * [Buy Premium Version](http://awplife.com/account/signup/flickr-gallery-premium/ "Buy Premium Version")
> * [Profile Gallery](http://demo.awplife.com/flickr-gallery-premium/ "Profile Gallery")
> * [Photostream Gallery](http://demo.awplife.com/flickr-gallery-premium/photo-stream-gallery/ "Photostream Gallery")
> * [Album Gallery](http://demo.awplife.com/flickr-gallery-premium/album-gallery/ "Album Gallery")
> * [Flickr Column Gallery](http://demo.awplife.com/flickr-gallery-premium/columns-gallery/ "Flickr Column Gallery")
> * [Flickr Photostream & Album Column Gallery](http://demo.awplife.com/flickr-gallery-premium/photo-stream-album-gallery/ "Flickr Photostream & Album Column Gallery")
> * [Widget Gallery](http://demo.awplife.com/flickr-gallery-premium/widget-gallery/ "Widget Gallery")
> * [Flickr Gallery Lightbox](http://demo.awplife.com/flickr-gallery-premium/flickr-gallery-light-box/ "Flickr Gallery Lightbox")
> * [Flickr Hover Transition Effect](http://demo.awplife.com/flickr-gallery-premium/flickr-hover-effects/ "Flickr Hover Transition Effect")
> * [Flickr Shadow & Glow Effect](http://demo.awplife.com/flickr-gallery-premium/flickr-shadow-glow-effect/ "Flickr Shadow & Glow Effect")
> * [Flickr Gallery Into Footer](http://demo.awplife.com/flickr-gallery-premium/flickr-gallery-into-footer/ "Flickr Gallery Into Footer")
> * [See More plugins](http://awplife.com/plugins/ "See More plugins")

**Get Premium Version With More Features**

* Pagination Settings
* Responsive Flickr Profile
* Photostream & Album Gallery & Flickr Profiles
* Flickr Photostream Gallery Shortcode
* Flickr Album Gallery Shortcode
* Multiple Responsive Lightbox With Thumbnails
* Image Sizing Settings With Flickr Cropping
* Thumbnails Into Light Box
* Gallery Title Settings 
* Multiple Columns Layout large desktops, desktops ,tablets, mobiles
* Infinitely customizable with custom CSS field
* Insert multiple gallery on same page with slideshow
* Multiple Page / Post / Widget & Footer Gallery
* 30+ Hover Effects For Album & Photostream
* Flickr Responsive Lightbox, Fixed Light Box, Lightgallery Lightbox, Light case Lightbox
* Create multiple galleries
* Diffrents Lightbox Image Sizing
* LightBox Transition Effects
* Lightcase slide Effect 
* Lightbox Title And Icon Unlimited Colors 
* Gallery Thumbnail Image Sizing
* Multiple galleries with lightbox

**Upgrade To Premium Plugin - <a href="http://awplife.com/product/flickr-gallery-wordpress-plugin/">Click Here</a>**

**Check Premium Plugin Demo - <a href="http://demo.awplife.com/flickr-gallery-premium/">Click Here</a>**

== Installation ==

Install Flickr Gallery either via the WordPress.org plugin directory or by uploading the files to your server.

After activating Flickr Gallery plugin, go to plugin menu.

Login into WordPress admin dashboard. Go to menu: Flickr Gallery --> Flickr API Settings

Add Your User ID & API Key 

Also  Go to menu: Flickr Gallery --> Add Flickr Gallery

Create gallery & configure settings and save.

Copy shortcode and paste shortcode into any Page / Post / Widget & Footer. And view page for gallery output.

That's it. You're ready to go!

== Screenshots ==

1. Flickr Photostream and Album gallery
2. Photostream Gallery & Multiple Album Gallery into Widgets
3. Multiple Gallery Into Footer
4. Multiple Album Gallery Into Page
5. large desktop lightbox Preview
6. lightcase lightbox Preview
7. lightbox for mobile phones
8. Buy Premium Plugin To get This Type of Profile & Follow Button settings for Flickr Gallery
9. Buy Premium Plugin To get Hover Effects Into Flickr Gallery
10. Flickr User Id & API Key Settings
11. Flickr Shortcode 

== Frequently Asked Questions ==

Have any queries?

Please post your question on plugin support forum

https://wordpress.org/support/plugin/wp-flickr-gallery/

== Changelog ==

= 0.0.16 =

* Enhancements: tested for wordpress 4.9
* Bug Fix: None
* Additional changes: yes, Added theme menu Added Featured Page. 

= 0.0.15 =

* Enhancements: tested for wordpress 4.9
* Bug Fix: None
* Additional changes: yes, Added theme menu. 

Feature Enhancements: Version 0.0.14
* Enhancements: Tested Upto New version 4.8.2
* Bug Fix: Fixed
* Additional changes: None

Feature Enhancements: Version 0.0.13
* Enhancements: Tested Upto New version 4.8.1
* Bug Fix: Fixed
* Additional changes: None

Feature Enhancements: Version 0.0.12
* Enhancements: Tested Upto New version 4.8.1
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.11
* Enhancements: compatible upto new version 4.8
* Bug Fix: Yes
* Additional changes: Changes into plugin name

Feature Enhancements: Version 0.0.10
* Enhancements: compatible upto new version 4.8
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.9
* Enhancements: compatible upto new version 4.8
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.8
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.7
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.6
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.5
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.4
* Enhancements: New Screen Shots
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.3
* Enhancements: New Screen Shots
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.2
* Enhancements: New Screen Shots
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.1
* Enhancements: None
* Bug Fix: None
* Additional changes: None

== Upgrade Notice ==
This is an initial release. Start with version 0.0.1. and share your feedback <a href="https://wordpress.org/support/view/plugin-reviews/wp-flickr-gallery/">here</a>.