<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aqualisys');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}W%_!/6 .TFA,txu$K#1qkDp9&E%ILS}E<UZx_=Q+y5]ZL7h,&1wZ9O8dfHTk>y~');
define('SECURE_AUTH_KEY',  'K Q4HH9g)J`T?GA<_#ri,V,$f9{f2,-e<!!buFW(Ae#4-`^(%2gR$OW:3R(2Yvue');
define('LOGGED_IN_KEY',    'p<krZ9Tf//e<dp]#^^G=5A4eN-.72xfB)-K;C>7+8H!6$T2Hk^V),L2.7*_(R>H+');
define('NONCE_KEY',        '>%b1D01DdnZthR:P[GG0G{_9JZH9oB;$(q1)qx^d=.|z/CI]-,q8=3MDgKNpCWft');
define('AUTH_SALT',        'ql9^H~|I}d=8Gj{9$ mjHyK3JCi(bsA9qkOJfMpz~dk@.!m9e*GNEIc~Ix)]<lc;');
define('SECURE_AUTH_SALT', '1D/a9kZTfx7{63j%,i<:.h&)`kA-li}r<eE4^<@h9v#fcO3-KuCNUTE5NI;w(X8k');
define('LOGGED_IN_SALT',   'AQNN.owNqeZ{u#~gzH~c~M2|#MUC,4/#L}7dl3w,xC:Td:qo9=Ep]`jpj^R/)&Q9');
define('NONCE_SALT',       ')g>#_[icXHhATD6@G~Yq&,?Yh9Z[B=M@{u#dS**P[Z!K^yoj`[|>6+h2*!Vpf^~W');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
